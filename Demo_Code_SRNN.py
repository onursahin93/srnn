# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 11:56:38 2018

@author: Safa Onur Şahin

NOTE : This code is not the final version of the SRNN code. This code is for demo purpose since it has pretrained model. 
"""

import unicodedata
import string
import re
import random
import time
import math
import scipy

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch import optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
dtype = torch.float
#%%

class EdgeRNN(nn.Module):
    def __init__(self, input_size, hidden_size, rnn_id, n_layers=1):
        super(EdgeRNN, self).__init__()
        
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.rnn_id = rnn_id

        self.EdgeRNN = nn.LSTMCell(input_size, hidden_size)
        
    def forward(self, xt, htct_1):
        ht_1, ct_1 = htct_1

        ht_Edge, ct_Edge = self.EdgeRNN(xt, (ht_1, ct_1))

        return ht_Edge, ct_Edge
    
    def init_hidden(self):

        ht_In = torch.randn(1, self.hidden_size, dtype=dtype).float()
        ct_In = torch.randn(1, self.hidden_size, dtype=dtype).float()
        ht_Edge = torch.randn(1, self.hidden_size, dtype=dtype).float()
        ct_Edge = torch.randn(1, self.hidden_size, dtype=dtype).float()
        ht_Node = torch.randn(1, self.hidden_size, dtype=dtype).float()
        ct_Node = torch.randn(1, self.hidden_size, dtype=dtype).float()
            
        return (ht_In, ct_In, ht_Edge, ct_Edge, ht_Node, ct_Node)
#%%
class InputRNN(nn.Module):
    def __init__(self, input_size, hidden_size, rnn_id, n_layers=1):
        super(InputRNN, self).__init__()
        
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.rnn_id = rnn_id

        self.inputRNN = nn.LSTMCell(input_size, hidden_size)
        
    def forward(self, xt, htct_1):
        ht_1, ct_1 = htct_1
        ht_In, ct_In = self.inputRNN(xt, (ht_1, ct_1))

        return  ht_In, ct_In
        

#%%
class NodeRNN(nn.Module):
    def __init__(self, input_size, hidden_size, rnn_id, n_layers=1):
        super(NodeRNN, self).__init__()
        
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.rnn_id = rnn_id

        self.nodeRNN = nn.LSTMCell(input_size, hidden_size)
        
    def forward(self, xt, htct_1):
        ht_1, ct_1 = htct_1
        ht_Node, ct_Node = self.nodeRNN(xt, (ht_1, ct_1))

        return  ht_Node, ct_Node
        
#%% 
class MyGraph(nn.Module):
    def __init__(self, node2class, input_size, hidden_size, n_layers=1):
        super(MyGraph, self).__init__()
        
        self.init_type = 'randn'
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers        
        self.node2class = node2class
        
        self.numNodes = len(self.node2class) 
        self.n_class = len(set(self.node2class))
        self.nodeRNN_input_size = self.hidden_size*self.n_class   
        self.nodeRNN_hidden_size = self.hidden_size*self.n_class        
        
        self.class2node = [[] for __ in range(self.n_class)]

        for i in range(len(self.node2class)):
            self.class2node[self.node2class[i]].append(i)
            
        self.endNodeClass2EdgeRnns = [[] for __ in range(self.n_class)]

        for i in range(self.n_class):
            for j in range(self.n_class):
                self.endNodeClass2EdgeRnns[i].append(j*self.n_class + i)
        
        self.numNodesPerClass = []        
        for i in range(self.n_class):
            self.numNodesPerClass.append(self.node2class.count(i))
        
#       TODO: single-double RNN option between classes 
        if(True): #double
            self.numEdgeRNNs = self.n_class*self.n_class
            #self.numEdgeRNNs = self.n_class*(self.n_class+1)/2
        
        self.NodeRNNList = []
        self.EdgeRNNList = []
        self.InputRNNList = []
        self.FCList = []
        self.EdgeRNNInputWeightList = []
        
#       TODO:FClist -> FC for each class
#       self.FC = torch.nn.Linear(self.nodeRNN_hidden_size, 1);   
#        for i in range(self.n_class):
#            self.FCList.append((torch.nn.Linear(self.nodeRNN_hidden_size, 1)))
        
        for i in range(self.numNodes):
            self.FCList.append((torch.nn.Linear(self.nodeRNN_hidden_size, 1))) 
        
        for i in range(self.n_class):
            self.InputRNNList.append((InputRNN(self.input_size, self.hidden_size, rnn_id = i)))
            
        for i in range(self.numEdgeRNNs):
            self.EdgeRNNList.append((EdgeRNN(self.input_size, self.hidden_size, rnn_id = i)))  
        
        for i in range(self.n_class):
            self.NodeRNNList.append((NodeRNN(self.nodeRNN_input_size, self.nodeRNN_hidden_size, rnn_id = i)))
        
        for start_class in range(self.n_class):
            for end_class in range(self.n_class):
                self.EdgeRNNInputWeightList.append(torch.nn.Linear(self.numNodesPerClass[start_class], self.input_size))
        
        
        self.H_In = Variable(torch.zeros(self.numNodes, self.hidden_size))
        self.H_Edge = Variable(torch.zeros(self.numNodes, self.n_class, self.hidden_size))
        self.H_Node = Variable(torch.zeros(self.numNodes, self.nodeRNN_hidden_size))
        
        self.C_In = Variable(torch.zeros(self.numNodes, self.hidden_size))
        self.C_Edge = Variable(torch.zeros(self.numNodes, self.n_class, self.hidden_size))
        self.C_Node = Variable(torch.zeros(self.numNodes, self.nodeRNN_hidden_size))
        
        self.X_Node = Variable(torch.zeros(self.numNodes, self.nodeRNN_input_size))
            
    def init_hidden(self):
        
        if(self.init_type == 'zeros'):
            H_In = Variable(torch.zeros(self.numNodes, self.hidden_size))
            H_Edge = Variable(torch.zeros(self.numNodes, self.n_class, self.hidden_size))
            H_Node = Variable(torch.zeros(self.numNodes, self.nodeRNN_hidden_size))
            
            C_In = Variable(torch.zeros(self.numNodes, self.hidden_size))
            C_Edge = Variable(torch.zeros(self.numNodes, self.n_class, self.hidden_size))
            C_Node = Variable(torch.zeros(self.numNodes, self.nodeRNN_hidden_size))
        else:
            H_In = Variable(0.01*torch.randn(self.numNodes, self.hidden_size))
            H_Edge = Variable(0.01*torch.randn(self.numNodes, self.n_class, self.hidden_size))
            H_Node = Variable(0.01*torch.randn(self.numNodes, self.nodeRNN_hidden_size))
            
            C_In = Variable(0.01*torch.randn(self.numNodes, self.hidden_size))
            C_Edge = Variable(0.01*torch.randn(self.numNodes, self.n_class, self.hidden_size))
            C_Node = Variable(0.01*torch.randn(self.numNodes, self.nodeRNN_hidden_size))
        
        return H_In, H_Edge, H_Node, C_In, C_Edge, C_Node
            
        
    def forward(self, X, restart_hidden):
        
        if(True):
            self.H_In, self.H_Edge, self.H_Node, self.C_In, self.C_Edge, self.C_Node  = self.init_hidden()
        
        outputs = Variable(torch.zeros(X.shape[0], self.numNodes, 1))
        
        self.seq_len = X.shape[0]        
        
        for t in range(self.seq_len):
            #takes the inputs at time t
            Xt = torch.index_select(X, 0, torch.tensor([t])).squeeze(0)
            
###############################################################################           
            #InputRNNs, process
            for node in range(self.numNodes):
                xt = torch.index_select(Xt, 0, torch.tensor([node]))
                ht_1 = torch.index_select(self.H_In, 0, torch.tensor([node]))
                ct_1 = torch.index_select(self.C_In, 0, torch.tensor([node]))
                ht, ct = self.InputRNNList[self.node2class[node]](xt, (ht_1,ct_1))
                self.H_In[node] = ht
                self.C_In[node] = ct
            
###############################################################################
            # EdgeRNNs
            # Input preparation
            #firsty, zero the X_Edge
            self.X_Edge = Variable(torch.zeros(self.n_class, self.input_size))                       
            # preparation of inputs of EdgeRNNs             
            for class_i in range(self.n_class):
                for start_node in self.class2node[class_i]:
                    xt = torch.index_select(Xt, 0, torch.tensor([start_node])).squeeze(0)
                    self.X_Edge[class_i] += xt              
            
            # process the EdgeRNNs
            for end_node in range(self.numNodes):
                xt = torch.index_select(Xt, 0, torch.tensor([end_node])).squeeze(0)
                endNodeClass = self.node2class[end_node]
                EdgeRNNs_node = self.endNodeClass2EdgeRnns[endNodeClass]
                #X_Edge_node = Variable(torch.zeros_like(X_Edge))
                
                # TODO : do this operation self.X_Edge[endNodeClass] -= xt without using inplace function            
                #self.X_Edge[endNodeClass] -= xt
                Ht_1_node = torch.index_select(self.H_Edge, 0, torch.tensor([end_node])).squeeze(0)                
                Ct_1_node = torch.index_select(self.C_Edge, 0, torch.tensor([end_node])).squeeze(0)  
                
                for edge_rnn in EdgeRNNs_node:
                    class_index = int(edge_rnn/self.n_class)
                    xt_edgeRnn = self.X_Edge[class_index].unsqueeze(0)
                    hx_1 = torch.index_select(Ht_1_node, 0, torch.tensor([class_index]))
                    cx_1 = torch.index_select(Ct_1_node, 0, torch.tensor([class_index]))
                    hx, cx = self.EdgeRNNList[edge_rnn](xt_edgeRnn, (hx_1, cx_1))
                    self.H_Edge[end_node, class_index] = hx.squeeze(0)
                    self.C_Edge[end_node, class_index] = cx.squeeze(0)
                    #outputs[t,node,0] = self.FC(ht)                 
                #self.X_Edge[endNodeClass] += xt
            #self.X_Edge = X_Edge
###############################################################################
            # NodeRNNs
            # Input preparation
            # firsty, zero the X_Node
            self.X_Node = Variable(torch.zeros(self.numNodes, self.hidden_size*self.n_class))

            # preparation of inputs of NodeRNNs
            for end_node in range(self.numNodes):
                for class_i in range(self.n_class):
                    if(class_i == 0):
                        x_temp = self.H_Edge[end_node, class_i]
                    else:
                        x_temp = torch.cat((x_temp, self.H_Edge[end_node, class_i]), dim=0)
                self.X_Node[end_node] = x_temp   
            
            #NodeRNNs, process
            for node in range(self.numNodes):
                xt = torch.index_select(self.X_Node, 0, torch.tensor([node]))
                ht_1_NodeRNN = torch.index_select(self.H_Node, 0, torch.tensor([node]))
                ct_1_NodeRNN = torch.index_select(self.C_Node, 0, torch.tensor([node]))
                ht_Node, ct_Node = self.NodeRNNList[self.node2class[node]](xt, (ht_1_NodeRNN, ct_1_NodeRNN))
                self.H_Node[node] = ht_Node.squeeze(0)
                self.C_Node[node] = ct_Node.squeeze(0)
                
                outputs[t,node,0] = torch.sigmoid(self.FCList[node](ht_Node.squeeze(0)))             
                
        return  outputs
        
#%%
def confusion(prediction, truth):
    confusion_vector = prediction / truth

    true_positives = torch.sum(confusion_vector == 1).item()
    false_positives = torch.sum(confusion_vector == float('inf')).item()
    true_negatives = torch.sum(torch.isnan(confusion_vector)).item()
    false_negatives = torch.sum(confusion_vector == 0).item()

    return true_positives, false_positives, false_negatives, true_negatives

def confusion_batch(prediction, truth):
    confusion_vector = prediction / truth

    true_positives = torch.sum(torch.sum(confusion_vector == 1)).item()
    false_positives = torch.sum(torch.sum(confusion_vector == float('inf'))).item()
    true_negatives = torch.sum(torch.sum(torch.isnan(confusion_vector))).item()
    false_negatives = torch.sum(torch.sum(confusion_vector == 0)).item()

    return true_positives, false_positives, false_negatives, true_negatives        
#%%
data_ = scipy.io.loadmat('crime_data.mat')    
data = data_['dataset']
X_ = torch.from_numpy(data).float() > 0.5
X_ = X_.float()
X_ = X_.transpose(0,1)
X_all = X_[:-1].unsqueeze(2)
Y_all = X_[1:].unsqueeze(2)

X_train = 2*X_all-1
Y_train = Y_all
#%%   
        
#%%
input_size = X_train.shape[2]
hidden_size = 20
eta = 0.001


#%% 
node2class_ = []
for i in range(X_train.shape[1]):
    node2class_.append(i)
    
n_class = len(set(node2class_))
numNodes = len(node2class_) 
#%%
net = MyGraph(node2class = node2class_, input_size= input_size, hidden_size=hidden_size)  
criterion = torch.nn.BCELoss()

load_pretrained = True        
if(load_pretrained):
    net = MyGraph(node2class = node2class_, input_size= input_size, hidden_size=hidden_size)  
    for i in range(len(net.InputRNNList)):
        net.InputRNNList[i].load_state_dict(torch.load('saved_model/modelInputRNN_'+ str(i) + '_saved.ckpt'))
    
    for i in range(len(net.EdgeRNNList)):
        net.EdgeRNNList[i].load_state_dict(torch.load('saved_model/modelEdgeRNN_'+ str(i) + '_saved.ckpt'))
    
    for i in range(len(net.NodeRNNList)):
        net.NodeRNNList[i].load_state_dict(torch.load('saved_model/modelNodeRNN_'+ str(i) + '_saved.ckpt'))
    
    for i in range(len(net.FCList)):
        net.FCList[i].load_state_dict(torch.load('saved_model/modelFC_'+ str(i) + '_saved.ckpt'))
#%%
parameters = set()
for net_ in net.InputRNNList:
    parameters |= set(net_.parameters())
    
for net_ in net.EdgeRNNList:
    parameters |= set(net_.parameters())
    
for net_ in net.NodeRNNList:
    parameters |= set(net_.parameters())
    
for net_ in net.FCList:
    parameters |= set(net_.parameters())
 
#%%
optimizer = optim.Adam(parameters, lr=eta)
#%%
batch_len = 30
numBatches = 12
restart_hidden = True
#%%
train_model = False
if(train_model):
    losses = []
    for ep in range(20):
        t0 = time.time()
        loss_epoch = 0
    #    Y_epoch = []
        for batch in range(numBatches):
    #        if(batch == 0):
    #            restart_hidden = True
    #        else:
    #            restart_hidden = False
                
            X_batch = X_train[batch*batch_len:(batch+1)*batch_len]
            Y_batch = Y_train[batch*batch_len:(batch+1)*batch_len]
            y_pred = net(X_batch, restart_hidden)
            loss = criterion(y_pred, Y_batch)
            net.zero_grad()
            optimizer.zero_grad()    
            loss.backward()
            optimizer.step()
            loss_epoch += loss.item()
        losses.append(loss_epoch)
        print('elapsed time at epoch ' + str(ep) + ' is '+  str(time.time() - t0))
    plt.plot(losses)

#%%
print('Showing the Confusion Matrix and AUC score for batches. The first ' +  str(numBatches) + ' batches are for training and the rest is for test.')
with torch.no_grad():
    for batch in range(60):
        X_batch_ = X_train[batch*batch_len:(batch+1)*batch_len]
        Y_batch_ = Y_train[batch*batch_len:(batch+1)*batch_len]
        y_pred_ = net(X_batch_, restart_hidden)
        predicted = y_pred_ > 0.5
        predicted = predicted.float()
        labels_batch = Y_batch_
        fpr, tpr, thresholds = metrics.roc_curve(Y_batch_.contiguous().view(-1,1).numpy(), y_pred_.contiguous().view(-1,1).numpy(), pos_label=1)
        auc_score = metrics.auc(fpr, tpr)
        print('Confusion matrix at batch ' + str(batch) + ' : '   + str(confusion_batch(predicted, labels_batch)) + ', -- AUC score :' + str(auc_score))


#%%
batch = 10
print('Showing the Confusion Matrix and AUC score for batch ' + str(batch))

with torch.no_grad():
    correct = 0
    total = 0
    batch = 50
    X_batch_ = X_train[batch*batch_len:(batch+1)*batch_len]
    Y_batch_ = Y_train[batch*batch_len:(batch+1)*batch_len]
    y_pred_ = net(X_batch_, restart_hidden)
    predicted = y_pred_ > 0.33
    predicted = predicted.float()
    labels_batch = Y_batch_
    fpr, tpr, thresholds = metrics.roc_curve(Y_batch_.contiguous().view(-1,1).numpy(), y_pred_.contiguous().view(-1,1).numpy(), pos_label=1)
    auc_score = metrics.auc(fpr, tpr)
    print('Confusion matrix at batch ' + str(batch) + ' : '   + str(confusion_batch(predicted, labels_batch)) + ', -- AUC score :' + str(auc_score))



node = 40
print('Showing the predictions for node ' + str(node) + ' of batch' + str(batch))
print('class : ', node2class_[node])
plt.figure(figsize = [10,10]);
plt.stem(Y_batch_[:,node])
plt.stem(y_pred_[:,node].detach().numpy(), 'r')


#%%

save_model = False
if(save_model):
    #not efficient but the only solution I have found    
    for i in range(len(net.InputRNNList)):
        torch.save(net.InputRNNList[i].state_dict(), 'saved_model/modelInputRNN_' + str(i)+ '_saved.ckpt')
    
    for i in range(len(net.EdgeRNNList)):
        torch.save(net.EdgeRNNList[i].state_dict(), 'saved_model/modelEdgeRNN_' + str(i)+ '_saved.ckpt')
    
    for i in range(len(net.NodeRNNList)):
        torch.save(net.NodeRNNList[i].state_dict(), 'saved_model/modelNodeRNN_' + str(i)+ '_saved.ckpt')
    
    for i in range(len(net.FCList)):
        torch.save(net.FCList[i].state_dict(), 'saved_model/modelFC_' + str(i)+ '_saved.ckpt')
        
#load_pretrained = False        
#if(load_pretrained):
#    net2 = MyGraph(node2class = node2class_, input_size= input_size, hidden_size=hidden_size)  
#    for i in range(len(net2.InputRNNList)):
#        net2.InputRNNList[i].load_state_dict(torch.load('saved_model/modelInputRNN_'+ str(i) + '_saved.ckpt'))
# 
#    for i in range(len(net2.EdgeRNNList)):
#        net2.EdgeRNNList[i].load_state_dict(torch.load('saved_model/modelEdgeRNN_'+ str(i) + '_saved.ckpt'))
#
#    for i in range(len(net2.NodeRNNList)):
#        net2.NodeRNNList[i].load_state_dict(torch.load('saved_model/modelNodeRNN_'+ str(i) + '_saved.ckpt'))
#    
#    for i in range(len(net2.FCList)):
#        net2.FCList[i].load_state_dict(torch.load('saved_model/modelFC_'+ str(i) + '_saved.ckpt'))